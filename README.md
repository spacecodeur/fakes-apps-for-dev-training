# Bienvenue !

Ce repo contient diverses simples briques/applications fakes permettant de tester ou d'illustrer facilement certains concepts en programmation.

**Pour les apprenants :** suivez les consignes données par votre enseignant(e)/formateur(rice)

**Pour les formateurs/rices :** le fichier `README_teacher.md` contient toutes les informations générales didactiques/pédagogiques de ce repo

