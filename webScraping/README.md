Le but de cette activité est de faire ces premiers pas, dans un cadre tout à fait légal, dans le monde du scraping. 
Elle permet également de ce faire la main sur un outil de création de tests, très utilisé en entreprise, qui est cypress.

# Le web scraping, qu'est-ce que donc ?

Le Web scraping (parfois appelé 'aspiration de données') désigne l'ensemble des techniques employées pour récupérer des données sur un site Web à l'aide d'**un script** ou d'**un programme**. 

Le Web scraping peut être utilisé pour : 
- récupérer et agréger des données de plusieurs sites pour créer son propre site, par exemple : sites de comparateur de prix (billet d'avion, ...)
- récupérer et exploiter des données, par exemple : récupérer l'évolution du cours d'une cryptomonnaie pour tenter de faire des prédictions avec un peu de machin learning 
- récupérer les méta-données et contenus de sites pour créer un moteur de recherche, par exemple : les robots utilisés par google pour le référencement 

Attention par contre, selon la source des données et leurs ré-utilisations (utiliser des données personnelles publiques ou encore créer [un site miroir](https://fr.wikipedia.org/wiki/Site_miroir)), le Web scraping peut être **une pratique illégale**. Si le sujet vous intéresse, voici quelques ressources utiles décrivant le cadre légal et non légal de cette pratique en France : 

- ré-utilisation de données personnelles, même exposées publiquement, est interdit : 
    - (2020) billet de la CNIL : [lien](https://www.cnil.fr/fr/la-reutilisation-des-donnees-publiquement-accessibles-en-ligne-des-fins-de-demarchage-commercial)
    - (~2019) résumé des principaux points à respecter : [lien](https://finddatalab.com/web-scraping-legal)
- (2023) [vidéo](https://www.youtube.com/watch?v=yAOIZ1Ei2z0) (~60mn) & [l'article](https://solo.stabler.tech/blog/is-web-scraping-legal) rataché sur le cadre général de la légalité/illégalité du Web scraping

## Comment scraper ? (et en toute légalité)

Nous avons besoin de deux choses

### Un site web pour s'entraîner à scraper 

Il existe des sites web créés exprès pour s'entrainer à scraper, en voici une petite liste : 

- http://books.toscrape.com
- ~ à compléter ~
- sans oublier vos propres sites persos sur lesquels vous pouvez vous entraîner

### Un outil pour nous aider à créer son script pour scraper

Imaginez vous en train de parcourir un site, de consulter son contenu et de cliquer ici et là, tout cela péniblement avec vos doigts d'être humain. Et bien, vous allez pouvoir automatiser toutes ces actions à l'aide d'une script.

Pour créer votre script de scraping, vous utiliserez ici [Cypress](https://www.cypress.io/) qui est un outils de test open-source très utilisé pour créer des tests end-to-end ( = tester les fonctionnalités d'un site Web). 

Tester un site Web n'est, en soi, pas du scraping. Mais ce type d'outil est assez pratique pour créer un scrapeur.

#### Installer cypress

L'installation de cypress est très simple : 

- Créer un nouveau dossier nommé "scrapingTraining"
- Se déplacer dedans et initialiser un projet nodejs avec la commande `npm init`
- Puis installer le package cypress via npm avec `npm install cypress`
- Puis démarrer le logiciel cypress avec la commande `npx cypress open`

Et c'est tout ! 

- Une fenêtre s'ouvre et vous propose soit d'effecter des tests E2E (end-to-end) ou des tests de composant : choisissez les tests E2E
- Cypress ajoute ensuite quelques fichier dans votre projet "scrapingTraining", cliquez sur suivant
- Ensuite choisissez un navigateur et cliquez sur 'Start E2E Testing in [navigateur sélectionné]'
- Sur l'écran suivant "Create your first spec" : cliquez sur "Create new spec", choisissez comme chemin et nom de fichier "cypress\e2e\\**[à changer : nom du site à scraper par exemple]**.cy.js"

Cette dernière étape permet de créer (enfin) notre script qui contiendra le code permettant de scraper notre site Web. 

**~ à partir d'ici nous allons attendre que tout le monde soit arrivé ici avant d'attaquer tous ensemble la suite de cette activité ~**

Pour les personnes en avance : 
- regardez autour de vous si des collègues ont besoin d'un coup de main
- consulter les (très) nombreuses ressources sur Internet expliquant comment cypress fonctionne. Intéressez vous tout particulièrement aux commandes qui se trouvent dans le bloc "Selection Commands" et "Action Commands" de ce [Cheat Sheet](https://cheatography.com/aiqbal/cheat-sheets/cypress-io/)
- creuser les aspects légaux/illégaux de la pratique du web scraping
- allez corriger des quêtes ;)