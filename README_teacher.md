# Organisation générale du répo

Le repo est comme une boite géante de lego : libre à vous de piocher dedans pour utiliser les briques que vous souhaitez dans le cadre de vos activités pédagogiques

Certaines activités sont déjà détaillées, vous trouverez ci-dessous le résumé de chaque activité ainsi qu'un lien vers un fichier d'explications plus détaillé

## Activités disponibles

### Journée d'Halloween

- **détaillé de l'activité** : [lien](activities/0.md)
- **public cible** : apprenants en DWWM 
- **objectif général** : s'initier au scraping et aux attaques type 'bruteforce' sur un formulaire HTTP
    - initiation à un outils de création de tests (cypress)
    - initiation au scraping
    - initiation à la sécurité : les attaques brute force sur un formulaire
- **compétences requises** :
    - essentiellement en javascript :
        - savoir utilisez les instructions de base : `if`, `for`
        - gestion de variables simples (str, int, ...) et complexes (tableaux, objets)
    - connaissances sur l'architecture client/serveur
    - connaissances du protocole HTTP : requetes get/post, code HTTP (200, 400, ...)
- **durée de l'activité** :
    - étalée tout au long d'une journée