const express = require('express')
const cors = require('cors');
const app = express()
const port = 42007

app.use(express.json());
app.use(cors());

function deniedAccessMessage(message = 'Accès refusé !')
{
  return message
}

function authorizedAccessMessage(message = 'Accès dévérouillé !')
{
  return message
}

// simple
app.post('/submit-form0', (req, res) => {
  const formData = req.body;

  if (formData.password !== '243') {
    return res.status(400).json({ message: deniedAccessMessage() });
  }

  return res.json({ message: authorizedAccessMessage() });
});

// + captcha (tres) sommaire
app.post('/submit-form1', (req, res) => {
  const formData = req.body;

  if (parseInt(formData.number1) + parseInt(formData.number2) !== parseInt(formData.captcha))
  {
    return res.status(400).json({ message: deniedAccessMessage('Captcha invalide !') });
  }

  if (formData.password !== '243') {
    return res.status(400).json({ message: deniedAccessMessage() });
  }

  return res.json({ message: authorizedAccessMessage() });
});

app.post('/submit-form2', (req, res) => {
  const formData = req.body;

  if (parseInt(formData.number1) + parseInt(formData.number2) !== parseInt(formData.captcha))
  {
    return res.status(400).json({ message: deniedAccessMessage('Captcha invalide !') });
  }

  if (formData.email !== 'admin@admin.com')
  {
    return res.status(400).json({ message: deniedAccessMessage('Email non valide !') }); 
  }

  if (formData.password !== '446')
  {
    return res.status(400).json({ message: deniedAccessMessage('Password non valide !') }); 
  }

  return res.json({ message: authorizedAccessMessage() });
});


// https://github.com/danielmiessler/SecLists/tree/master/Passwords/Common-Credentials
// https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-10000.txt
app.post('/submit-form3', (req, res) => {
  const formData = req.body;

  if (parseInt(formData.number1) + parseInt(formData.number2) !== parseInt(formData.captcha))
  {
    return res.status(400).json({ message: deniedAccessMessage('Captcha invalide !') });
  }

  if (formData.email !== 'admin@admin.com')
  {
    return res.status(400).json({ message: deniedAccessMessage('Email non valide !') }); 
  }

  if (formData.password !== 'brazil')
  {
    return res.status(400).json({ message: deniedAccessMessage('Password non valide !') }); 
  }

  return res.json({ message: authorizedAccessMessage() });
});

// password hexadecimal
app.post('/submit-form4', (req, res) => {
  const formData = req.body;

  if (parseInt(formData.number1) + parseInt(formData.number2) !== parseInt(formData.captcha))
  {
    return res.status(400).json({ message: deniedAccessMessage('Captcha invalide !') });
  }

  if (formData.email !== 'admin@admin.com')
  {
    return res.status(400).json({ message: deniedAccessMessage('Email non valide !') }); 
  }

  if (formData.password !== '11e5a34304')
  {
    return res.status(400).json({ message: deniedAccessMessage('Password non valide !') }); 
  }

  return res.json({ message: authorizedAccessMessage() });
});

// no help, good luck !
// email avec que des lettres
// password hexa
app.post('/submit-form5', (req, res) => {
  const formData = req.body;

  if (parseInt(formData.number1) + parseInt(formData.number2) !== parseInt(formData.captcha))
  {
    console.log('test1');
    return res.status(400).json({ message: deniedAccessMessage('Captcha invalide !') });
  }
  
  if (formData.email !== 'foo@faatruc.com' || formData.password !== '11e5a34304')
  {
    console.log('test2');
    return res.status(400).json({ message: deniedAccessMessage() }); 
  }

  console.log('test3');
  return res.json({ message: authorizedAccessMessage() });
});


app.listen(port, () => {
  console.log('Serveur Backend UP !')
})
