Cette application met à dispotition divers formulaires d'authentification.
Ces formulaires peuvent par exemple permettre à un administrateur de se connecter sur un backoffice pour pouvoir effectuer des opérations sensibles..


## Comment instaler et déployer ce projet ?

- un `git clone` de ce repos (ou téléchargement du repo sous un fichier .zip)
- se diriger avec son terminal dans le dossier `formulaires/authentification`
- exécuter la commande `npm install`
- puis exécuter la commande `npm run start:app`

Et vous devriez avoir dans le terminal l'adresse IP sur laquelle votre robot doit commencer son travail !

Le but du jeu ici est de réussir à trouver les bonnes informations à rentrer dans les formulaire (ordonné par ordre de difficulté). Évidemment, cela prendrait un temps considérable si vous cherchez à trouver les bonnes informations demandées manuellement. 
Vous devez donc passer par un script ! 

Pour terminer, les informations nécessaires pour réussir à passer les formulaires sont écrites en dur dans le dossier back. Vous pouvez explorer le code pour les trouver, mais le but du jeu reste avant tout de créer des scripts pour réussir à passer les formulaires tout seul comme des grands

## Information sur les formulaires 

### Formulaire de sécurité niveau 0 : 

Il s'agit d'un formulaire simple dans lequel il faut rentrer le bon code pour dévérouiller l'accès. 
Petite indication : le password est un code de 3 chiffres :) 

### Formulaire de sécurité niveau 1 : 

En plus du formulaire précédent : un captcha (très) simple s'est glissé dans le formulaire ! Est-ce que votre script sera assez intelligent pour résoudre son énigme diabolique ?

### Formulaire de sécurité niveau 2 : 

Diantre ! Un champ mail s'est maintenant incrusté dans le formulaire...
Je suis sur que c'est un mail pas super compliqué à trouver...il doit y avoir le mot admin utilisé dedans, et si on testais manuellement un peu pour trouver le bon mail ? 

### Formulaire de sécurité niveau 3 :

Le mail est toujours le même...Mais l'administrateur a détecté nos précédentes intrusions et il a alors mis un mot de passe plus compliqué. 
Je suis sur qu'il utilise un mot de passe courant ! 
Et si je récupérais les mots de passes dans [ce petit fichier](// https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-10000.txt) et que je les soumets tous un à un au formulaire ?

### Formulaire de sécurité niveau 4 :

Damned ! 
Le développeur du site s'est énervé ! maintenant il est impossible de connaitre le mail utilisé ni le mot de passe ! le formulaire ne donne plus aucune information. 
A tous les coup, son mot de passe est maintenant composé d'exactement 10 chiffres et lettres (hexadécimales, un truc de d'informaticien)
Par contre, aucune information sur le bon mail...

Cette partie va être ardue !