import { NavLink, Outlet } from "react-router-dom";

import "./App.css";

function App() {
  return (
    <>
      <h1>Formulaires d'authentification</h1>
      <nav>
        <NavLink to="/" activeclassname="active">Home</NavLink>
        <NavLink to="/authentification0" activeclassname="active">Authentification : sécurité niveau 0</NavLink>
        <NavLink to="/authentification1" activeclassname="active">Authentification : sécurité niveau 1</NavLink>
        <NavLink to="/authentification2" activeclassname="active">Authentification : sécurité niveau 2</NavLink>
        <NavLink to="/authentification3" activeclassname="active">Authentification : sécurité niveau 3</NavLink>
        <NavLink to="/authentification4" activeclassname="active">Authentification : sécurité niveau 4</NavLink>
      </nav>
      <main>
        <hr />
        <Outlet />
      </main>
    </>
  );
}

export default App;
