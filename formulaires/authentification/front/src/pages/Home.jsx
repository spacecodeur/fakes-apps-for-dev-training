import { useState, useEffect } from "react";
import Markdown from 'react-markdown';
import MarkdownFile from '../../../README.md';

function Home() {

  const [markdown, setMarkdown] = useState("");

  useEffect(() => {
    fetch(MarkdownFile)
      .then((res) => res.text())
      .then((text) => setMarkdown(text));
  }, []);

  return (
    <>
      <Markdown className='markdown'>{markdown}</Markdown>
    </>
  );
}

export default Home;
