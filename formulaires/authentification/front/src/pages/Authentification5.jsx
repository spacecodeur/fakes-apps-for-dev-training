import { useState } from "react";

const number1 = Math.floor(Math.random() * 10) + 1
const number2 = Math.floor(Math.random() * 10) + 1

function Authentification0() {
  const [formData, setFormData] = useState({
    number1: number1,
    number2: number2,
    captcha: "",
    email: "",
    password: "",
  });
  const [formMessage, setFormMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch("http://localhost:42007/submit-form5", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      const data = await response.json();
      setFormMessage(data.message);
      
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  return (
    <>
      <h2>Authentification : sécurité niveau 0</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group captcha">
          <label htmlFor="captcha">Captcha : </label>
          <input
            type="text"
            name="number1"
            value={number1}
            readOnly="readonly"
            onChange={handleInputChange}
            required
          />+
          <input
            type="text"
            name="number2"
            value={number2}
            readOnly="readonly"
            onChange={handleInputChange}
            required
          />=
          <input
            type="text"
            name="captcha"
            value={formData.captcha}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group email">
          <label htmlFor="email">Email : </label>
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group password">
          <label htmlFor="password">Password : </label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group">
          <button type="submit">Submit</button>
        </div>
      </form>
      <div>{formMessage !== "" && <p>{formMessage}</p>}</div>
    </>
  );
}

export default Authentification0;
