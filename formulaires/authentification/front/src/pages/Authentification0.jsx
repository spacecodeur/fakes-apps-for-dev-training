import { useState } from "react";

function Authentification0() {
  const [formData, setFormData] = useState({
    password: "",
  });
  const [formMessage, setFormMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch("http://localhost:42007/submit-form0", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      const data = await response.json();
      setFormMessage(data.message);
      
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  return (
    <>
      <h2>Authentification : sécurité niveau 0</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group password">
          <label htmlFor="password">Password : </label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group">
          <button type="submit">Submit</button>
        </div>
      </form>
      <div>{formMessage !== "" && <p>{formMessage}</p>}</div>
    </>
  );
}

export default Authentification0;
