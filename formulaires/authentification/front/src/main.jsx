import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import App from "./App";

// page components

import Home from "./pages/Home";
import Authentification0 from "./pages/Authentification0";
import Authentification1 from "./pages/Authentification1";
import Authentification2 from "./pages/Authentification2";
import Authentification3 from "./pages/Authentification3";
import Authentification4 from "./pages/Authentification4";
import Authentification5 from "./pages/Authentification5";

// router creation

const router = createBrowserRouter([
  {
    element: <App />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/authentification0",
        element: <Authentification0 />,
      },
      {
        path: "/authentification1",
        element: <Authentification1 />,
      },
      {
        path: "/authentification2",
        element: <Authentification2 />,
      },
      {
        path: "/authentification3",
        element: <Authentification3 />,
      },
      {
        path: "/authentification4",
        element: <Authentification4 />,
      },
      {
        path: "/authentification5",
        element: <Authentification5 />,
      },
    ],
  },
]);

// rendering

ReactDOM.createRoot(document.getElementById("root")).render(
  <RouterProvider router={router} />
);
